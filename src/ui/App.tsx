import React, { lazy, Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import Loader from './components/loader/Loader';
import styles from './App.module.scss';
import Header from './components/header/Header';
import ErrorBoundary from './components/errorBoundary/ErrorBoundary';
// code splitting
/*************************************************************/
const loadQuoteContainer = () => import('./pages/quote/QuoteContainer');
const loadQuoteFormContainer = () =>
  import('./pages/quote-form/QuoteFormContainer');

const QuoteContainer = lazy(loadQuoteContainer);
const QuoteFormContainer = lazy(loadQuoteFormContainer);
// component
/*************************************************************/
const App = () => (
  <div>
    <Header />
    <main className={styles.main}>
      <ErrorBoundary
        error={
          'Something went wrong, please try refresh the page or contact the support team'
        }
        title={'Javascript error'}
      >
        <Suspense fallback={<Loader />}>
          <Switch>
            <Route exact path={'/'} component={QuoteFormContainer} />
            <Route
              path={'/quote/:from/:to/:amount'}
              component={QuoteContainer}
            />
            <Route component={QuoteFormContainer} />
          </Switch>
        </Suspense>
      </ErrorBoundary>
    </main>
  </div>
);

export default App;
