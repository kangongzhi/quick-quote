import React from 'react';
import styles from './Input.module.scss';

const Input = ({
  type = 'text',
  value,
  handleOnChange = () => {},
  handleOnBlur = validations => () => {},
  handleOnFocus = () => {},
  label = '',
  name = '',
  id = '',
  validations = {},
  valid = undefined,
  className = '',
  inputClassName = '',
  readOnly = false
}) => (
  <div className={`relative flex ${className}`}>
    <label htmlFor={id} className={'hide'}>
      {label}
    </label>
    <input
      type={type}
      name={name}
      placeholder={label}
      value={value}
      onChange={handleOnChange}
      onBlur={handleOnBlur(validations)}
      onFocus={handleOnFocus}
      id={id}
      className={`${styles.input} ${
        valid === false ? styles.error : ''
      } ${inputClassName}`}
      readOnly={readOnly}
    />
  </div>
);

export default Input;
