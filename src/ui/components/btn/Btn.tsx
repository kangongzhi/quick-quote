import React from 'react';
import styles from './Btn.module.scss';

const Btn = ({ children, handleClick, className = '', disabled = false }) => (
  <button
    onClick={handleClick}
    className={`${styles.btn} ${className}`}
    disabled={disabled}
  >
    {children}
  </button>
);

export default Btn;
