import styles from './Select.module.scss';
import React from 'react';

interface Option {
  value: string;
  display: string;
}

interface Props {
  handleOnChange: any;
  options: Option[];
  name: string;
  value: string;
  className?: string;
}

const Select = ({
  handleOnChange,
  options,
  name,
  value,
  className = ''
}: Props) => (
  <select
    className={`${styles.select} ${className}`}
    value={value}
    onChange={handleOnChange}
    name={name}
  >
    {options.map(({ value, display }, index) => (
      <option value={value} key={index}>
        {display}
      </option>
    ))}
  </select>
);

export default Select;
