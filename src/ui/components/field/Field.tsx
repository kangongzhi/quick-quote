import React from 'react';
import star from './star.svg';
import styles from './Field.module.scss';
// component - HOC
/*************************************************************/
const Field = ({ extraProp, ...passThroughProps }) => {
  const { valid = undefined } = passThroughProps;
  const { title, error = '', isRequired = false, Component } = extraProp;

  return (
    <div className={styles.field}>
      <div className={`flex ${styles['title-container']}`}>
        <div className={styles.title}>{title}</div>
        {isRequired === true && (
          <img src={star} className={styles.star} alt={'required'} />
        )}
      </div>
      <Component {...passThroughProps} />
      {valid === false && <div className={styles.error}>{error}</div>}
    </div>
  );
};

export default Field;
