import styled from 'styled-components';

const Page = styled.div`
  width: 100%;
`;

Page.displayName = 'Page';

export default Page;
