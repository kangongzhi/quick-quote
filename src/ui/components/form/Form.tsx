import React from 'react';
// component - HOC
/*************************************************************/
const Form = ({ WrappedComponent, ...passThroughProps }) => (
  <WrappedComponent {...passThroughProps} />
);

export default Form;
