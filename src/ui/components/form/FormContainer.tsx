import validate from '../../../helper/validator';
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { updateForm } from '../../../redux/form';
import { connect } from 'react-redux';
import Form from './Form';
import { Form as FormType } from '../../../redux/form';
// helpers - handle input change
/*************************************************************/
// todo validation types
const changeHandler = updateForm => ({ target: { name, value } }) => {
  updateForm({
    name,
    value: value.trim()
  });
};
const blurHandler = updateForm => (validations: {}) => ({
  target: { name, value }
}) => {
  const validationResult = validate(value.trim(), validations);
  updateForm({
    name,
    valid: validate(value.trim(), validations) === true,
    error: validationResult === true ? '' : validationResult
  });
};
// helpers - types
/*************************************************************/
interface Props {
  form: FormType;
  updateForm: any;
  WrappedComponent: Component;
}
// component - HOC
/*************************************************************/
const FormContainer = ({ updateForm, ...passThroughProps }: Props) => (
  <Form
    {...passThroughProps}
    handleOnChange={changeHandler(updateForm)}
    handleOnBlur={blurHandler(updateForm)}
  />
);
// connect to store
/*************************************************************/
const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateForm: updateForm
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FormContainer);
