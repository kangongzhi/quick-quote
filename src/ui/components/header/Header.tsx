import styles from './Header.module.scss';
import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const StyledLink = styled(Link)`
  text-decoration: none;
`;

const Header = () => (
  <header className={`${styles.header} flex flex-column justify-center`}>
    <StyledLink to={'/'}>
      <h3 className={styles.title}>Quick Quote</h3>
    </StyledLink>
    <hr className={styles.line}></hr>
  </header>
);

export default Header;
