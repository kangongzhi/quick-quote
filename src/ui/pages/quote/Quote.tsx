import React from 'react';
import styled from 'styled-components';
import Btn from '../../components/btn/Btn';
import Page from '../../components/page/Page';
import styles from './Quote.module.scss';
import Result, { Title } from './result/Result';

const NewQuoteBtn = styled(Btn)`
  margin-top: 38px;
`;

const Quote = ({
  query: { from, to, amount },
  quote: { customerRate, customerAmount },
  handleClick
}) => (
  <Page
    className={`flex flex-column justify-center items-center ${styles.page}`}
  >
    <div>
      <div className={`flex flex-column ${styles['rate-container']}`}>
        <Title>OFX Customer Rate</Title>
        <div className={styles['customer-rate']}>{customerRate}</div>
      </div>
      <Result title={'From'} amount={amount} currency={from} />
      <Result title={'To'} amount={customerAmount} currency={to} />
    </div>
    <NewQuoteBtn handleClick={handleClick}>START NEW QUOTE</NewQuoteBtn>
  </Page>
);

export default Quote;
