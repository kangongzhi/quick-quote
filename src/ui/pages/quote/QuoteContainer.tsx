import React, { useEffect, useState } from 'react';
import Quote from './Quote';
import { getQuote, Response } from '../../../helper/apis/get-quote';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { resetQuote, updateQuote } from '../../../redux/quote';
import { updateForm } from '../../../redux/form';
import { Redirect } from 'react-router-dom';
import Loader from '../../components/loader/Loader';
import { Error } from '../../components/errorBoundary/ErrorBoundary';
// helper
/*************************************************************/
const parse = ({ CustomerRate, CustomerAmount }: Response) => ({
  customerRate: CustomerRate,
  customerAmount: CustomerAmount
});

const handleClick = (setBtnClicked, resetQuote) => () => {
  resetQuote();
  setBtnClicked(true);
};

const renderContent = (btnClicked, query, quote, setBtnClicked, resetQuote) =>
  btnClicked === true ? (
    <Redirect to={'/'} />
  ) : (
    <Quote
      query={query}
      quote={quote}
      handleClick={handleClick(setBtnClicked, resetQuote)}
    />
  );

const saveError = setError => ({ message }) => {
  setError(message);
};
// component
/*************************************************************/
const QuoteContainer = ({ query, updateQuote, quote, resetQuote }) => {
  const [btnClicked, setBtnClicked] = useState(false);
  const [fetching, setFetching] = useState(true);
  const [error, setError] = useState('');

  useEffect(() => {
    getQuote(query)
      .then(parse)
      .then(updateQuote)
      .finally(() => {
        setTimeout(() => {
          setFetching(false);
        }, 300);
      })
      .catch(saveError(setError));
  }, []);

  if (error !== '') {
    return <Error error={error} title={'HTTP Error'} />;
  }

  return fetching === true ? (
    <Loader />
  ) : (
    renderContent(btnClicked, query, quote, setBtnClicked, resetQuote)
  );
};
// connect to store
/*************************************************************/
const mapStateToProps = (
  { quote },
  {
    match: {
      params: { from, to, amount }
    }
  }
) => ({
  query: { from, to, amount },
  quote: quote
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateQuery: updateForm,
      updateQuote: updateQuote,
      resetQuote: resetQuote
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuoteContainer);
