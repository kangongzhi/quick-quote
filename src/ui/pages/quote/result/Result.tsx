import styled from 'styled-components';
import React from 'react';
import { prettyAmount } from '../../../../helper/helper';
import styles from './Result.module.scss';

export const Title = styled.div`
  color: #5e5e5e;
  font-size: 22px;
  margin-bottom: 6px;
`;

Title.displayName = 'Title';

const Currency = styled.div`
  color: #535353;
`;

const Amount = styled.div`
  color: #6185a1;
  margin-left: 10px;
`;

const Result = ({ title, currency, amount }) => (
  <div className={styles.result}>
    <Title>{title}</Title>
    <div className={'flex'}>
      <Currency>{currency.toUpperCase()}</Currency>
      <Amount>{prettyAmount(amount + '')}</Amount>
    </div>
  </div>
);

export default Result;
