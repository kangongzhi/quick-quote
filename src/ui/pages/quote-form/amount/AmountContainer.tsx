import Input from '../../../components/input/Input';
import React, { useState } from 'react';
import { prettyAmount } from '../../../../helper/helper';
// helpers - state management
/*************************************************************/
const handleOnFocus = setOnFocus => () => {
  setOnFocus(true);
};

const handleOnBlurWrapper = (
  handleOnBlur,
  setOnFocus
) => validations => event => {
  handleOnBlur(validations)(event);
  setOnFocus(false);
};
// component
/*************************************************************/
const AmountContainer = ({ handleOnBlur, valid, value, ...rest }) => {
  const [onFocus, setOnFocus] = useState(false);
  return onFocus || valid !== true ? (
    <Input
      // @ts-ignore
      handleOnBlur={handleOnBlurWrapper(handleOnBlur, setOnFocus)}
      valid={valid}
      value={value}
      {...rest}
    />
  ) : (
    <Input
      value={prettyAmount(value)}
      handleOnFocus={handleOnFocus(setOnFocus)}
      readOnly={true}
    />
  );
};

export default AmountContainer;
