import React from 'react';
import Input from '../../components/input/Input';
import Field from '../../components/field/Field';
import styles from './QuoteForm.module.scss';
import ContactNumber from './contactNumber/ContactNumber';
import Select from '../../components/select/Select';
import AmountContainer from './amount/AmountContainer';
import QuoteBtnContainer from './quoteBtn/QuoteBtnContainer';
import Page from '../../components/page/Page';

const Grid = ({ className = '', children }) => (
  <div className={`clearfix ${className}`}>{children}</div>
);

const Row = ({ className = '', children }) => (
  <div className={`sm-col ${className}`}>{children}</div>
);

const FormField = props => {
  const {
    field: { value, error, touched, valid },
    isRequired,
    Component,
    title,
    ...others
  } = props;

  const propsPassThrough = { ...others, value, touched, valid };

  return (
    <Field
      extraProp={{
        title,
        error,
        isRequired,
        Component
      }}
      {...propsPassThrough}
    />
  );
};

const QuoteForm = ({
  form: {
    firstName,
    lastName,
    email,
    countryCode,
    phoneNumber,
    from,
    to,
    amount
  },
  handleOnChange,
  handleOnBlur
}) => (
  <Page>
    <form className={styles.form}>
      <Grid className={styles['personal-info']}>
        <Row className={'sm-col-5'}>
          <FormField
            field={firstName}
            title={'First Name'}
            isRequired={true}
            Component={Input}
            id={'qf-first-name'}
            label={'First Name'}
            name={'firstName'}
            handleOnChange={handleOnChange}
            handleOnBlur={handleOnBlur}
            validations={{ required: true }}
          />
        </Row>
        <Row className={`sm-col-5 ${styles['last-name']}`}>
          <FormField
            field={lastName}
            title={'Last Name'}
            isRequired={true}
            Component={Input}
            id={'qf-last-name'}
            label={'Last Name'}
            name={'lastName'}
            handleOnChange={handleOnChange}
            handleOnBlur={handleOnBlur}
            validations={{ required: true }}
          />
        </Row>
        <Row className={'sm-col-12'}>
          <FormField
            field={email}
            title={'Email'}
            Component={Input}
            id={'qf-email'}
            label={'Email'}
            name={'email'}
            handleOnChange={handleOnChange}
            handleOnBlur={handleOnBlur}
            validations={{ email: true }}
          />
        </Row>
        <Row className={'sm-col-12'}>
          <Field
            extraProp={{
              title: 'Telephone/Mobile',
              Component: ContactNumber,
              error: phoneNumber.error
            }}
            valid={phoneNumber.valid}
            handleOnChange={handleOnChange}
            handleOnBlur={handleOnBlur}
            countryCode={countryCode}
            phoneNumber={phoneNumber}
            countryCodeName={'countryCode'}
            phoneNumberName={'phoneNumber'}
            options={[
              { value: '61', display: '+61' },
              { value: '86', display: '+86' }
            ]}
          />
        </Row>
      </Grid>
      <div className={styles['quote-info']}>
        <Field
          extraProp={{
            title: 'From Currency',
            Component: Select,
            isRequired: true
          }}
          handleOnChange={handleOnChange}
          options={[
            { value: 'aud', display: 'Australian dollars(AUD)' },
            { value: 'usd', display: 'American dollars(USD)' }
          ]}
          name={'from'}
          value={from.value}
        />
        <Field
          extraProp={{
            title: 'To Currency',
            Component: Select,
            isRequired: true
          }}
          handleOnChange={handleOnChange}
          options={[
            { value: 'aud', display: 'Australian dollars(AUD)' },
            { value: 'usd', display: 'American dollars(USD)' }
          ]}
          name={'to'}
          value={to.value}
        />
        <FormField
          field={amount}
          title={'Amount'}
          Component={AmountContainer}
          type={'number'}
          isRequired={true}
          id={'qf-amount'}
          label={'Amount'}
          name={'amount'}
          handleOnChange={handleOnChange}
          handleOnBlur={handleOnBlur}
          validations={{ amount: true, required: true }}
        />
      </div>
      <div className={`flex justify-center items-center ${styles.btn}`}>
        <QuoteBtnContainer />
      </div>
    </form>
  </Page>
);

export default QuoteForm;
