import styles from './ContactNumber.module.scss';
import Input from '../../../components/input/Input';
import React from 'react';
import Select from '../../../components/select/Select';
// component
/*************************************************************/
const ContactNumber = ({
  handleOnChange,
  handleOnBlur,
  options = [],
  countryCodeName,
  phoneNumberName,
  valid,
  countryCode: { value: countryCodeValue },
  phoneNumber: { value: phoneNumberValue }
}) => (
  <div>
    <div className={'flex'}>
      <Select
        value={countryCodeValue}
        handleOnChange={handleOnChange}
        name={countryCodeName}
        options={options}
        className={styles.select}
      />
      <Input
        type={'number'}
        label={'phone number'}
        value={phoneNumberValue}
        name={phoneNumberName}
        id={'qf-phone-number'}
        handleOnBlur={handleOnBlur}
        handleOnChange={handleOnChange}
        validations={{ phoneNumber: true }}
        valid={valid}
        className={styles['number-container']}
        inputClassName={styles['number-input']}
      />
    </div>
  </div>
);

export default ContactNumber;
