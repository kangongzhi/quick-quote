import React from 'react';

const QuoteBtn = ({ handleClick }) => (
  <button onClick={handleClick}>Get quote</button>
);

export default QuoteBtn;
