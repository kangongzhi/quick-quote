import React from 'react';
import { bindActionCreators } from 'redux';
import { updateForm } from '../../../../redux/form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getQuery, isValid } from '../../../../selectors/form';
import Btn from '../../../components/btn/Btn';
// helper
/*************************************************************/
const quoteUrl = ({ from, to, amount }) => `/quote/${from}/${to}/${amount}`;

const handleClick = (history, query) => () => {
  history.push(quoteUrl(query));
};
// component
/*************************************************************/
const QuoteBtnContainer = ({ history, query, isFormValid }) => (
  <Btn
    handleClick={handleClick(history, query)}
    disabled={isFormValid === false}
  >
    GET QUOTE
  </Btn>
);
// connect to store
/*************************************************************/
const mapStateToProps = ({ form }) => ({
  query: getQuery(form),
  isFormValid: isValid(form)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateForm: updateForm
    },
    dispatch
  );

export default withRouter(
  // @ts-ignore
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(QuoteBtnContainer)
);
