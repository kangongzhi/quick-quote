import React from 'react';
import QuoteForm from './QuoteForm';
import { bindActionCreators } from 'redux';
import { updateForm } from '../../../redux/form';
import { connect } from 'react-redux';
import FormContainer from '../../components/form/FormContainer';
// component
/*************************************************************/
const QuoteFormContainer = ({ form }) => (
  // @ts-ignore
  <FormContainer form={form} WrappedComponent={QuoteForm} />
);
// connect to store
/*************************************************************/
const mapStateToProps = ({ form }) => ({
  form: form
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateForm: updateForm
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuoteFormContainer);
