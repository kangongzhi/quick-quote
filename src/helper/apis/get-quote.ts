import axios from 'axios';
import { endpoint } from './api-setup';
// response type
/*************************************************************/
export interface Response {
  ComparisonAmount: number;
  ComparisonRate: number;
  CustomerAmount: number;
  CustomerRate: number;
  CustomerRateInverse: number;
  DefaultFee: number;
  DeliveryCountry: string;
  DeliveryTime: number;
  Fee: number;
  FeeFreeThreshold: number;
  InterbankAmount: number;
  InterbankRate: number;
  InverseInterbankRate: number;
  Message: string;
}
// url
/*************************************************************/
const quoteLink = (from: string, to: string, amount: number) =>
  `${endpoint}/${from}/${to}/${amount}?format%20=json`;
// api
/*************************************************************/
export const getQuote = ({ from, to, amount }): Response | any =>
  axios.get(quoteLink(from, to, amount));
