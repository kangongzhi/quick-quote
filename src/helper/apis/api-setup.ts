import axios from 'axios';
// interceptors
/*************************************************************/
const resResolveInterceptor = ({ data }: any) => data;
const resRejectInterceptor = (err: any) => Promise.reject(err);

axios.interceptors.response.use(resResolveInterceptor, resRejectInterceptor);
// end point
/*************************************************************/
export const endpoint =
  'https://api.ofx.com/PublicSite.ApiService/OFX/spotrate/Individual';
