// helpers - validator
/*************************************************************/
const minLengthValidator = (value, minLength: number) =>
  value.length >= minLength;

const emailValidator = (value: string) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(value).toLowerCase());
};

const numberValidator = value => /^\d+$/.test(value);

const isValidLeadingZero = value => value[0] === '0' && value[1] === '.';
const noLeadingZero = value => value[0] !== '0';
const amountValidator = (value: string) =>
  (noLeadingZero(value) || isValidLeadingZero(value)) &&
  isNaN(+value) === false;
// helpers - validators
/*************************************************************/
const minRequiredLength = 1;
const validators = {
  required: value => minLengthValidator(value, minRequiredLength),
  email: value => emailValidator(value),
  phoneNumber: value => numberValidator(value),
  amount: value => amountValidator(value)
};
const errorMsgs = {
  required: 'required',
  email: 'please type in a valid email',
  phoneNumber: 'please type in a valid phone number',
  amount: 'please type in a valid amount'
};
// validate
/*************************************************************/
const validate = (value, validations = {}): boolean | string => {
  if (validations['required'] === undefined && value === '') {
    return true;
  }
  for (let name in validations) {
    if (validators[name](value, validations[name]) === false) {
      return errorMsgs[name];
    }
  }

  return true;
};

export default validate;
