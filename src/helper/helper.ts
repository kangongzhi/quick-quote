// helpers - pretty amount
/*************************************************************/
const hasDecimal = amount => amount.indexOf('.') !== -1;

export const prettyAmount = (amount: string) => {
  const prettied = amount.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  return hasDecimal(amount) === true ? prettied : `${prettied}.00`;
};
