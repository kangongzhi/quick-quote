// actions creators
/*************************************************************/
const UPDATE = '@form/update';
// actions - types
/*************************************************************/
interface Field {
  name: string;
  value?: string;
  valid?: undefined;
  error?: string;
}

interface FormData {
  value?: string;
  valid?: undefined;
  error?: string;
  required?: boolean;
}

export interface Form {
  [name: string]: FormData;
}
// actions
/*************************************************************/
export const updateForm = (field: Field) => ({
  type: UPDATE,
  field
});
// reducer
/*************************************************************/
const initialState: Form = {
  firstName: {
    value: '',
    valid: undefined,
    error: '',
    required: true
  },
  lastName: {
    value: '',
    valid: undefined,
    error: '',
    required: true
  },
  email: {
    value: '',
    valid: undefined,
    error: ''
  },
  countryCode: {
    value: '',
    error: ''
  },
  phoneNumber: {
    value: '',
    valid: undefined,
    error: ''
  },
  from: {
    value: 'aud',
    required: true
  },
  to: {
    value: 'usd',
    required: true
  },
  amount: {
    value: '',
    valid: undefined,
    error: '',
    required: true
  }
};

const form = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE:
      const { name, ...rest } = action.field;
      return { ...state, [name]: { ...state[name], ...rest } };
    default:
      return state;
  }
};

export default form;
// export for unit testing
/*************************************************************/
export { initialState, UPDATE };
