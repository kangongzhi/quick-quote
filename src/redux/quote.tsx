// actions creators
/*************************************************************/
const RESET = '@quote/reset';
const UPDATE = '@quote/update';
// actions - types
/*************************************************************/
interface Quote {
  customerRate: number;
  customerAmount: number;
}
// actions
/*************************************************************/
export const updateQuote = (quote: Quote) => ({
  type: UPDATE,
  quote
});

export const resetQuote = () => ({
  type: RESET
});
// reducer
/*************************************************************/
const initialState = {};

const quote = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE:
      return { ...action.quote };
    case RESET:
      return initialState;
    default:
      return state;
  }
};

export default quote;
// export for unit testing
/*************************************************************/
export { initialState, UPDATE };
