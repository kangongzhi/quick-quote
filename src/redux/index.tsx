import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import form from './form';
import quote from './quote';

const reducers = { form, quote };

const createRootReducer = (history: any) =>
  combineReducers({ router: connectRouter(history), ...reducers });

export default createRootReducer;
// export for unit testing
/*****************************************/
export { reducers };
