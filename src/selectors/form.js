import { createSelector } from 'reselect';

const getForm = form => form;

export const getQuery = createSelector(
  getForm,
  ({
    from: { value: from },
    to: { value: to },
    amount: { value: amount }
  }) => ({ from, to, amount })
);

const hasEmptyRequiredValues = form => key =>
  form[key].required === true && form[key].value === '';

const errorFields = form => key =>
  form[key].error !== '' && form[key].error !== undefined;

export const isValid = createSelector(
  getForm,
  form => {
    const keys = Object.keys(form);
    return (
      keys.filter(hasEmptyRequiredValues(form)).length === 0 &&
      keys.filter(errorFields(form)).length === 0
    );
  }
);
